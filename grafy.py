from random import choice
from typing import Sequence, Dict, Tuple, List
import time
from numpy import random, arange


# noinspection PyShadowingNames
class Graph:
    def __init__(self):
        self.size = 0
        self.adjacency_list = {}
        self.colors = {}
        self.degrees = {}

    def __repr__(self):
        return str(self.adjacency_list)

    def clear_colors(self):
        self.colors = {vertex: 0 for vertex in range(1, self.size + 1)}

    def set_degrees(self):
        for index in range(1, self.size + 1):
            self.degrees[index] = len(self.adjacency_list[index])

    def complete_graph(self):
        complete = []
        for x in range(1, self.size + 1):
            for y in range(x + 1, self.size + 1):
                complete.append((x, y))
        return complete

    def generate(self, size, count):
        self.size = size
        self.clear_colors()
        complete = self.complete_graph()
        random_pairs = []

        for _ in range(count):
            pair = choice(complete)
            complete.remove(pair)
            random_pairs.append(pair)

        self.adjacency_list = {}
        for nr in range(1, size + 1):
            self.adjacency_list[nr] = []
        for elem in random_pairs:
            self.adjacency_list[elem[0]].append(elem[1])
            self.adjacency_list[elem[1]].append(elem[0])
        with open('graph.txt', 'w') as f:
            f.write(str(size) + '\n')
            for first, second in random_pairs:
                f.write(str(first) + ' ' + str(second) + '\n')
            f.close()

    def read_DIMACS_instance(self, filename):
        with open("./instances/" + filename) as file:
            self.size = 0
            for line in file:
                if line[0] == 'p':
                    self.size = int(line.split()[2])
                    break
            self.adjacency_list = {vertex: [] for vertex in range(1, self.size + 1)}
            for line in file:
                elem = line.split()
                vertex1 = int(elem[1])
                vertex2 = int(elem[2])
                self.adjacency_list[vertex1].append(vertex2)
                self.adjacency_list[vertex2].append(vertex1)
        self.clear_colors()

    def read_instance(self, filename):
        with open("./instances/" + filename) as file:
            self.size = int(file.readline())
            self.adjacency_list = {vertex: [] for vertex in range(1, self.size + 1)}
            for line in file:
                elem = line.split()

                vertex1 = int(elem[0])
                vertex2 = int(elem[1])
                self.adjacency_list[vertex1].append(vertex2)
                self.adjacency_list[vertex2].append(vertex1)
        self.clear_colors()

    def _find_greedy_color(self, vertex):
        neighbours_colors = [self.colors[x] for x in self.adjacency_list[vertex]]
        for color in range(1, self.size + 1):
            if color not in neighbours_colors:
                return color

    def greedy_coloring_order(self, order: Sequence[int]) -> Dict[int, int]:
        self.clear_colors()
        for vertex in order:
            self.colors[vertex] = self._find_greedy_color(vertex)
        return self.colors

    def greedy_color(self):
        return self.greedy_coloring_order(range(1, self.size + 1))

    def max_color(self):
        return max(self.colors.values())


    def genetic_coloring(self):

        def set_fitness(chromosome: Sequence[int]) -> int:
            self.greedy_coloring_order(chromosome)
            fitness = 0
            for vertex in range(1, self.size + 1):
                fitness += (self.colors[vertex] + self.size * self.max_color())
            return fitness

        def local_search_operations(chromosome: Tuple[int]) -> Tuple[int]:
            self.greedy_coloring_order(chromosome)
            independent_color_sets = [[] for _ in range(0, (self.max_color()) + 1)]  #preparing sublist for every color set

            for vertex, color in self.colors.items():
                independent_color_sets[color].append(vertex)

            independent_color_sets.sort(key=len, reverse=True)
            color = 0
            for independent_color_set in independent_color_sets:  #redrawing graph with new order
                color += 1
                for vertex in independent_color_set:
                    self.colors[vertex] = color
            new_chromosome = [item for sublist in independent_color_sets for item in sublist]
            return tuple(new_chromosome)

        def merging_crossover(first_parent: Sequence[int], second_parent: Sequence[int]) \
                -> Tuple[int]:
            first = list(first_parent)
            second = list(second_parent)
            merged = []
            max_index = len(first) - 1
            first_index = 0
            second_index = 0
            for _ in (range(len(first) * 2)):
                choice = random.randint(0, 2)
                if choice == 0 and first_index <= max_index:
                    merged.append(first[first_index])
                    first_index += 1
                elif second_index <= max_index:
                    merged.append(second[second_index])
                    second_index += 1
            child = []
            for element in merged:
                if element not in child:
                        child.append(element)
            return tuple(child)

        def order_crossover(first_parent: Sequence[int], second_parent: Sequence[int]) -> Tuple[int]:
            first = list(first_parent)
            second = list(second_parent)

            cross_point = int(len(first) / 3)
            child = first[cross_point: cross_point * 2]

            second_mid_part = second[cross_point: cross_point * 2]
            del second[cross_point: cross_point * 2]

            tmp = []
            for elem in second_mid_part:
                if elem not in child:
                    tmp.append(elem)
            child = tmp + child

            for elem in second:
                if elem not in child:
                    child.append(elem)
            return tuple(child)

        population = set()
        pop_size = 50  #--parameter--

        for _ in range(pop_size):
            chromosome = tuple(random.permutation(arange(1, self.size + 1)))
            for _ in range(5):
                chromosome = local_search_operations(chromosome)
            population.add(chromosome)

        for generations in range(100):  #--parameter--
            # selection
            fitness_rank = []
            for chromosome in population:
                fitness_rank.append([chromosome, set_fitness(chromosome)])

            fitness_rank = sorted(fitness_rank, key=lambda x: x[1])

            # crossover
            pop_size = len(population)-1
            for index in range(0, pop_size, 2):
                first_parent = fitness_rank[index][0]
                second_parent = fitness_rank[index + 1][0]

                first_child = merging_crossover(first_parent, second_parent)
                second_child = merging_crossover(second_parent, first_parent)

                first_parent_mutant = local_search_operations(first_parent)
                second_parent_mutant = local_search_operations(second_parent)

                fitness_rank.append([first_child, set_fitness(first_child)])
                fitness_rank.append([second_child, set_fitness(second_child)])
                fitness_rank.append([first_parent_mutant, set_fitness(first_parent_mutant)])
                fitness_rank.append([second_parent_mutant, set_fitness(second_parent_mutant)])

            fitness_rank = sorted(fitness_rank, key=lambda x: x[1])

            new_population = set()
            max_index = len(fitness_rank)//3
            index = 0
            while (index < max_index):
                if fitness_rank[index][0] not in new_population:
                    new_population.add(fitness_rank[index][0])
                else:
                    max_index += 1
                index += 1

            population = new_population

        chromatic_number = self.size
        for chromosome in population:
            self.greedy_coloring_order(chromosome)
            if chromatic_number > self.max_color():
                chromatic_number = self.max_color()
        return chromatic_number



start_time = time.time()

graph = Graph()
#graph.read_DIMACS_instance("queen6_6.col")

graph.read_instance("gc_1000_300013.txt")
#graph.gen(23, 99)
print(graph.genetic_coloring())
print("greedy: \n")
graph.greedy_color()
print(graph.max_color())

print("\n--- %s seconds ---" % (time.time() - start_time))
